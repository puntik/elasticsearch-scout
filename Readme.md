Experimental usage

Install laravel scout:

```shell
composer require laravel/scout

php artisan vendor:publish --provider="Laravel\Scout\ScoutServiceProvider"
```

```dotenv
SCOUT_DRIVER   = elasticsearch
SCOUT_PREFIX   = ""
SCOUT_QUEUE    = false
SCOUT_IDENTIFY = false
```

Clone submodule for elasticsearch engine.

```shell
git submodule add https://puntik@bitbucket.org/puntik/elasticsearch-scout.git
```

Register engine:

see: https://laravel.com/docs/8.x/scout#custom-engines

```php
<?php

use Abetzi\ElasticSearch\Scout\Driver;
use Elasticsearch\Client;
use Laravel\Scout\EngineManager;

/**
 * Bootstrap any application services.
 *
 * @return void
 */
public function boot()
{
    resolve(EngineManager::class)->extend('elasticsearch', function () {
        return new Driver($this->app->get(Client::class));
    });
}
```
